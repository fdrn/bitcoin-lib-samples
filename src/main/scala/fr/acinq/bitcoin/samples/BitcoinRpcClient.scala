package fr.acinq.bitcoin.samples

import akka.actor.ActorSystem
import akka.http.javadsl.model.HttpEntities
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import akka.http.scaladsl.settings.ConnectionPoolSettings
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import fr.acinq.bitcoin.{Base58Check, BinaryData, Crypto, Transaction}
import org.bouncycastle.util.Arrays
import org.json4s._
import org.json4s.jackson.{JsonMethods, Serialization}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try

/**
  * basic JSON-RPC client
  */
object JsonRpcClient {
  implicit val formats = Serialization.formats(NoTypeHints)

  case class Body(method: String, params: Seq[Any], id: Option[String] = None)

  def buildRequest(uri: String, method: String, params: Seq[Any]): HttpRequest = {
    val body = Body(method, params)
    val json = Serialization.write(body)
    HttpRequest(method = HttpMethods.POST, uri = uri).withEntity(HttpEntities.create(ContentTypes.`application/json`, json))
  }

  def toJValue(raw: String): JValue = {
    val json = JsonMethods.parse(raw)
    val error = json \ "error"
    if (error != JNull) throw new RuntimeException(s"cannot parse $raw: " + Serialization.write(error))
    json \ "result"
  }
}

object BitcoinRpcClient {

  case class BlockInfo(hash: String, confirmations: Int, size: Int, height: Int, version: Int, merkleroot: String, tx: Seq[String])

}

/**
  * basic bitcoind RPC client
  *
  * @param user     bitcoind user name
  * @param password bitcoind password
  * @param uri      bitdoinc RPC uri (typically it would be http://localhost:8332 on mainnet or http://localhost:18332 on testnet)
  */
class BitcoinRpcClient(user: String, password: String, host: String, port: Int)(implicit system: ActorSystem, mat: ActorMaterializer) {

  import BitcoinRpcClient._
  import org.json4s._
  import org.json4s.jackson.{JsonMethods, Serialization}

  implicit val formats = Serialization.formats(NoTypeHints)
  implicit val ec = system.dispatcher

  val settings = ConnectionPoolSettings(system).withMaxConnections(1).withMaxOpenRequests(8)
  val connectionFlow: Flow[HttpRequest, HttpResponse, Future[Http.OutgoingConnection]] = Http().outgoingConnection(host, port)

  def call(request: HttpRequest): Future[JValue] = {
    call(Seq(request)).map(_.head)
  }

  def toJvalue(response: HttpResponse): Future[JValue] = {
    response.entity.toStrict(1 second).map(_.data.toArray).map(new String(_)).map(JsonRpcClient.toJValue)
  }

  def call(requests: Seq[HttpRequest]) = {
    val responseFuture = Source.fromIterator(() => requests.iterator)
      .via(connectionFlow)
      .runFoldAsync(Seq.empty[JValue])({
        case (a, b) => toJvalue(b).map(j => a :+ j)
      })
    responseFuture
  }

  def call(method: String, params: Any*): Future[JValue] = {
    val request = JsonRpcClient.buildRequest("/", method, params).addHeader(Authorization(BasicHttpCredentials(user, password)))
    call(request)
  }

  def getBalance(account: String): Future[Double] = {
    val request = JsonRpcClient.buildRequest("/", "getbalance", account).addHeader(Authorization(BasicHttpCredentials(user, password)))
    call(request).map(_.extract[Double])
  }

  def getRawTx(txid: String): Future[String] = {
    val request = JsonRpcClient.buildRequest("/", "getrawtransaction", txid :: Nil).addHeader(Authorization(BasicHttpCredentials(user, password)))

    call(request).map(json => {
      json.extractOpt[String].getOrElse(throw new RuntimeException(s"cannot parse tx $txid from $json"))
    })
  }

  def getTx(txid: String): Future[Transaction] = getRawTx(txid).map(Transaction.read)

  def getRawTx(txids: Seq[String]) = {
    val requests = txids.map(txid => JsonRpcClient.buildRequest("/", "getrawtransaction", txid :: Nil).addHeader(Authorization(BasicHttpCredentials(user, password))))
    val foo = call(requests)
    val bar = foo.map(_.map(json => json.extractOpt[String].getOrElse(throw new RuntimeException(s"cannot parse tx from $json"))))
    bar.map(_.map(Transaction.read))
  }

  def getbestblockhash: Future[String] = {
    val request = JsonRpcClient.buildRequest("/", "getbestblockhash", Nil).addHeader(Authorization(BasicHttpCredentials(user, password)))
    call(request).map(_.extract[String])
  }

  def getblock(hash: String): Future[BlockInfo] = {
    val request = JsonRpcClient.buildRequest("/", "getblock", hash :: Nil).addHeader(Authorization(BasicHttpCredentials(user, password)))
    call(request).map(json => {
      json.extractOpt[BlockInfo].getOrElse(throw new RuntimeException(s"cannot parse block $hash from $json"))
    })
  }

  def getblockhash(height: Int): Future[String] = {
    val request = JsonRpcClient.buildRequest("/", "getblockhash", height :: Nil).addHeader(Authorization(BasicHttpCredentials(user, password)))
    call(request).map(_.extract[String])
  }

  def getblock(height: Int): Future[BlockInfo] = getblockhash(height).flatMap(getblock)
}


object BitcoinRpcClientSample extends App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  implicit val formats = Serialization.formats(NoTypeHints)

  val client = new BitcoinRpcClient("foo", "bar", "localhost", 18332)
  val future = for {
    balance <- client.getBalance("foo")
    txhash = "3bf8c518a7a1187287516da67cb96733697b1d83eb937e68ae39bd4c08e563b7"
    _ = println(s"balance = $balance")
    tx <- client.getTx(txhash)
    _ = println(s"$tx")
    json1 <- client.call("getnewaddress")
    address = json1.extract[String]
    (_, pubhash) = Base58Check.decode(address)
    json2 <- client.call("dumpprivkey", address)
    privkey = json2.extract[String]
    _ = println(s"address $address private key $privkey")
    (_, priv) = Base58Check.decode(privkey)
    pub = Crypto.publicKeyFromPrivateKey(priv)
    _ = assert(BinaryData(pub.hash) == BinaryData(pubhash))
  } yield ()

  Await.result(future, 5 seconds)

  Http().shutdownAllConnectionPools()
  materializer.shutdown()
  system.shutdown()
}
