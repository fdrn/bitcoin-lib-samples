package fr.acinq.bitcoin.samples

import fr.acinq.bitcoin.DeterministicWallet._
import fr.acinq.bitcoin._

object Bip44Wallet extends App {
  /**
    * this is how you can re-compute the keys and addresses used by
    * a BIP44 compliant wallet (Copay, ....) from its seed
    */

  val testnet = false

  // step #1: compute the seed from the mnemonic code
  //  val seed = MnemonicCode.toSeed(
  //    "mixed" :: "lonely" :: "good" :: "divorce" :: "scorpion" :: "foster" ::
  //      "ten" :: "gain" :: "raven" :: "earth" :: "wonder" :: "leader" :: Nil,
  //    passphrase = "")

  val seed = MnemonicCode.toSeed(
    "radar give kingdom cupboard drip account cannon hurdle time possible salmon divide".split(' '),
    passphrase = "")

  // step #2: generate the master key from the seed
  val master = generate(seed)
  println(s"master key: $master ${encode(master, testnet)}")

  // step #3: derive the account key from the master key
  val account = if (testnet)
    derivePrivateKey(master, hardened(44) :: hardened(1) :: hardened(0) :: Nil)
  else
    derivePrivateKey(master, /*hardened(44) :: hardened(0) :: */ hardened(0) :: Nil)

  val accountPub = publicKey(account)
  println(s"account public key: $accountPub ${encode(accountPub, testnet)}")

  // compute a few keys and addresses...
  for (i <- 0L to 100L) {
    val pub = DeterministicWallet.publicKey(DeterministicWallet.derivePrivateKey(account, 0L :: i :: Nil))
    //println(pub)
    val prefix = if (testnet) Base58.Prefix.PubkeyAddressTestnet else Base58.Prefix.PubkeyAddress
    println(Base58Check.encode(prefix, pub.publicKey.hash))
  }

  // 1BokS2i6DYDUTe5KHJNdy7LEaP4ewbNHmh
}
