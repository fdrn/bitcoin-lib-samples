package fr.acinq.bitcoin.samples

import java.io.ByteArrayOutputStream
import java.math.BigInteger
import java.util.Base64

import fr.acinq.bitcoin._
import org.bouncycastle.crypto.digests.SHA256Digest
import org.bouncycastle.crypto.signers.HMacDSAKCalculator

import scala.sys.process._
import scala.util.Random


object SignMessage extends App {
  val magic: BinaryData = "Bitcoin Signed Message:\n".getBytes

  def hash(message: BinaryData): BinaryData = {
    val data: BinaryData = {
      val bos = new ByteArrayOutputStream()
      Protocol.writeScript(magic, bos)
      Protocol.writeScript(message, bos)
      bos.toByteArray
    }
    val hash: BinaryData = Crypto.hash256(data)
    hash
  }

  def signMessage(message: BinaryData, key: BinaryData): String = {
    val h: BinaryData = hash(message)
    val (r, s) = Crypto.sign(h, key)
    val ar: BinaryData = fixSize(r.toByteArray)
    val as: BinaryData = fixSize(s.toByteArray)

    val gen = new HMacDSAKCalculator(new SHA256Digest)
    gen.init(Crypto.curve.getN, new BigInteger(1, key.take(32).toArray), h)
    val k = gen.nextK()
    val recid = if (Crypto.curve.getG.multiply(k).normalize.getAffineYCoord.toBigInteger.mod(Crypto.curve.getN).testBit(0)) 1 else 0

    val sig: BinaryData = (27 + 4 + recid).toByte +: (ar ++ as)
    val pub = Crypto.publicKeyFromPrivateKey(key)
    val address = Base58Check.encode(Base58.Prefix.PubkeyAddressTestnet, pub.hash)
    val encoded = new String(Base64.getEncoder.encode(sig), "UTF-8")
    for (i <- 0 to 4) {
      val sig1: BinaryData = sig.updated(0, (27 + 4 + i).toByte)
      val encoded = new String(Base64.getEncoder.encode(sig1), "UTF-8")
      println(s"testing $recid $i $encoded")
      val foo = s"bitcoin-cli verifymessage $address $encoded foobar" !!

      println(foo)
    }
    encoded
    //    ""
  }

  val (Base58.Prefix.SecretKeyTestnet, priv) = Base58Check.decode("cVqQrTokHWrXh49zmNGhC3Mf9VBQJQARqX5VUtkYHH6K5Gruwr74")
  val pub = Crypto.publicKeyFromPrivateKey(priv)
  val address = Base58Check.encode(Base58.Prefix.PubkeyAddressTestnet, pub.hash)


  println(address)
  signMessage("foobar".getBytes, priv)
  println(verifyMessage("foobar", "IF7sqvfAVW8/ZuJGStN2aFEY1sRf+lA4Vv8cufWcrOvMfF8Jd1zOjuNxzzqpMu/x7BFZfAIgflv3RdiDSeJhzrI=", pub))

  def verifyMessage(message: String, signature: String, pub: BinaryData): Boolean = {
    val bin = Base64.getDecoder.decode(signature)
    val r = new BigInteger(1, bin.drop(1).take(32))
    val s = new BigInteger(1, bin.drop(33).take(32))
    val h: BinaryData = hash(message.getBytes)
    val result = Crypto.verifySignature(h, (r, s), pub)
    result
  }

  def fixSize(in: Array[Byte]): Array[Byte] = in.size match {
    case 32 => in
    case s if s < 32 => Array.fill(32 - s)(0: Byte) ++ in
    case s if s > 32 => in.takeRight(32)
  }

  //  val priv = new Array[Byte](32)
  //  val random = new Random()
  //  for (i <- 0 until 10) {
  //    random.nextBytes(priv)
  //    val pub = Crypto.publicKeyFromPrivateKey(priv :+ 1.toByte)
  //    val address = Base58Check.encode(Base58.Prefix.PubkeyAddressTestnet, Crypto.hash160(pub))
  //    val sig = signMessage("foobar".getBytes, priv)
  //    val foo = s"bitcoin-cli verifymessage $address $sig foobar" !!
  //
  //    println(foo)
  //  }

}
