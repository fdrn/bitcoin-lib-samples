package fr.acinq.bitcoin.samples

import fr.acinq.bitcoin.{Block, Transaction}
import zmq.ZMQ

object ZmqListener extends App {
  val ctx = ZMQ.init(1)
  val sc = ZMQ.socket(ctx, ZMQ.ZMQ_SUB)
  sc.setSocketOpt(ZMQ.ZMQ_SUBSCRIBE, "rawblock")
  sc.setSocketOpt(ZMQ.ZMQ_SUBSCRIBE, "rawtx")
  val rc = ZMQ.connect(sc, "tcp://127.0.0.1:28332")
  // start bitcoind or bitcoin-qt with the following options:
  // -zmqpubrawtx=tcp://127.0.0.1:28332 -zmqpubrawblock=tcp://127.0.0.1:28332
  while (true) {
    val msg = sc.recv(0)
    if (msg.hasMore) {
      val topic = new String(msg.data())
      val msg1 = sc.recv(0)
      topic match {
        case "rawtx" =>
          val tx = Transaction.read(msg1.data())
          println(s"received tx $tx")
        case "rawblock" =>
          val block = Block.read(msg1.data())
          println(s"received block ${block.header}")
      }
    }
  }
  ZMQ.close(sc)
  ZMQ.term(ctx)
}
