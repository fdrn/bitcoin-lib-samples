package fr.acinq.bitcoin.samples

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.stream.ActorMaterializer
import fr.acinq.bitcoin._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object ValidateTx extends App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  val client = new BitcoinRpcClient("foo", "bar", "localhost", 18332) // 8332: mainnet 18332: tesnet

  def getTx(txhash: String): Future[Transaction] = {
    //    val request = HttpRequest(uri = s"https://blockchain.info/rawtx/$txhash?format=hex")
    //    Http().singleRequest(request).flatMap {
    //      response => response.entity.toStrict(10 second)
    //    }.map(_.data.toArray).map(new String(_)).map(Transaction.read)
    client.getTx(txhash)
  }


  def callback: Script.Runner.Callback = (script: List[ScriptElt], stack: Script.Stack, state: Script.Runner.State) => {
    println(s"current script: $script")
    println(s"stack: ${stack.map(BinaryData(_))}")
    println(s"state: $state")
    println
    true
  }

  def validateTx(txhash: String): Future[Unit] = {
    for {
      tx <- getTx(txId)
      previousTxIds = tx.txIn.map(_.outPoint.txid.toString)
      previousTxs <- Future.sequence(previousTxIds.map(getTx))
    } yield Transaction.correctlySpends(tx, previousTxs, ScriptFlags.STANDARD_SCRIPT_VERIFY_FLAGS, Some(callback))
  }

  val txId = "11ddecf05fc64aef0151e6740bb2f0d19ddd2a1c6158fdfa1796e1464152cb84"
  val future = validateTx(txId)
  Await.result(future, 10 minutes)
  Http().shutdownAllConnectionPools()
  system.shutdown()
}
