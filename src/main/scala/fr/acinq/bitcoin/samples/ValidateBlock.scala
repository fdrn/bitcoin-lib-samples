package fr.acinq.bitcoin.samples

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import fr.acinq.bitcoin.{ScriptFlags, Transaction}
import grizzled.slf4j.Logging
import org.json4s.NoTypeHints
import org.json4s.jackson.Serialization

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try

object ValidateBlock extends App with Logging {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  implicit val formats = Serialization.formats(NoTypeHints)

  val client = new BitcoinRpcClient("foo", "bar", "localhost", 18332) // 8332: mainnet 18332: tesnet

  def validateTx(txhash: String): Unit = {
    val future = for {
      tx <- client.getTx(txhash)
      previousTxIds = tx.txIn.map(_.outPoint.txid.toString).toSet.toSeq
      previousTxs <- client.getRawTx(previousTxIds)
    } yield {
      logger.debug(s"validating tx $txhash which spends from ${previousTxs.length} transactions")
      Try(Transaction.correctlySpends(tx, previousTxs, ScriptFlags.STANDARD_SCRIPT_VERIFY_FLAGS)).getOrElse(logger.error(s"cannot verify transaction $txhash"))
    }
    Await.result(future, 5 minutes)
  }

  def validateBlock(hash: String) = {
    val future = for {
      blockInfo <- client.getblock(hash)
      _ = logger.info(s"block $hash contains ${blockInfo.tx.length} transactions")
      results = blockInfo.tx.tail.map(validateTx)
    } yield results
    future
  }

  def validateBlock(height: Int) = {
    val future = for {
      blockInfo <- client.getblock(height)
      _ = logger.info(s"block ${blockInfo.hash} at height $height contains ${blockInfo.tx.length} transactions")
      results = blockInfo.tx.tail.map(validateTx)
    } yield results
    future
  }

  def validateBestBlock = {
    val future = for {
      hash <- client.getbestblockhash
      _ = logger.debug(s"best block hash $hash")
    } yield validateBlock(hash)
    future
  }

  for (height <- args(0).toInt to args(1).toInt) {
    val futures = validateBlock(height)
    Await.result(futures, 60 minute)
  }
  Http().shutdownAllConnectionPools()
  system.shutdown()

}
